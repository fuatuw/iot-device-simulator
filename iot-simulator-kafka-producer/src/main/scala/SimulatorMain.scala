import java.util.UUID.randomUUID
import java.util.{Calendar, Properties}

import com.google.gson.Gson
import com.iot.simulator.{Data, Device, DeviceSerializer, PropertyReader}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.slf4j.LoggerFactory

import scala.util.Random


/**
  * Created by farslan2 on 3/9/19.
  */
object SimulatorMain {

  val logger = LoggerFactory.getLogger(classOf[App])
  def generateDeviceIoTEvent(producer: KafkaProducer[String, Data], kafkaTopic: String) : Unit = {

    //Generate three different devices and put to list.
    val deviceIdList = List(randomUUID().toString, randomUUID().toString, randomUUID().toString)
    val rand = new Random()
    while (true) {
      var deviceId = deviceIdList(rand.nextInt(3))
      var temp = rand.nextInt(120) - 40
      var latitude =  rand.nextInt(90) + rand.nextFloat()
      var longitute = rand.nextFloat() + rand.nextInt(180) - 90
      var timestamp = Calendar.getInstance().getTimeInMillis
      var device = Device(deviceId, temp, latitude , longitute ,  timestamp )
      var data = Data(device)

      producer.send(new ProducerRecord[String, Data](kafkaTopic, data))
      println(s"${device.deviceId}, ${device.temperature}, ${device.latitude}, ${device.longitude}, ${device.timestamp}")
      val gson = new Gson()
      val deviceSerializer = new DeviceSerializer()
      logger.info(s"${new String(deviceSerializer.serialize("", data))} is sent...")

      Thread.sleep(1000)
    }



  }

  def main(args : Array[String]) {

    val props : Properties = new PropertyReader().readPropertyFile()

    val zookeeper = props.getProperty("com.iot.simulator.zookeeper")
    val brokers = props.getProperty("com.iot.simulator.kafka.boostrap.servers")
    val kafkaTopic = props.getProperty("com.iot.simulator.kafka.topic")

    //Producer properties
    val properties = new Properties()
    properties.put("bootstrap.servers", brokers)
    properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    properties.put("value.serializer", "com.iot.simulator.DeviceSerializer")

    val producer = new KafkaProducer[String, Data](properties)
    generateDeviceIoTEvent(producer, kafkaTopic)

  }

}
