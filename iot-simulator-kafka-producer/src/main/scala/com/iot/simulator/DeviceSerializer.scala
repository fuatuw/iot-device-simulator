package com.iot.simulator

import java.util

import com.google.gson.Gson
import kafka.utils.VerifiableProperties
import org.apache.kafka.common.serialization.Serializer

/**
  * Created by farslan2 on 3/9/19.
  */
class DeviceSerializer extends Serializer[Data] {

  def this(verifiableProperties: VerifiableProperties){this}

  override def serialize(s: String, dEvent: Data): Array[Byte] = {
    try {
      val gson = new Gson()
      val jsonDeviceEvent = gson.toJson(dEvent)
      jsonDeviceEvent.getBytes()
    }catch {
      case e:Exception =>
        throw e
    }
  }

  override def configure(map: util.Map[String, _], b: Boolean): Unit = {}


  override def close(): Unit = {}
}
