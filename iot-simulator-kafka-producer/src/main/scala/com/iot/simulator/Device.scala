package com.iot.simulator

/**
  * Created by farslan2 on 3/12/19.
  */
case class Data(data: Device) extends Serializable

case class Device(deviceId: String = "", temperature: Int = 0, latitude: Float = 0f, longitude: Float = 0f, timestamp: Long = 0L) extends Serializable


