package com.iot.processor


import java.text.SimpleDateFormat
import java.util.Properties

import com.iot.processor.util.{Data, Device, PropertyReader}
import _root_.kafka.serializer.StringDecoder
import com.google.gson.Gson
import org.apache.hadoop.hbase.{HBaseConfiguration, HColumnDescriptor, HTableDescriptor}
import org.apache.hadoop.hbase.client.{ HConnectionManager}
import org.apache.hadoop.hbase.client.{HBaseAdmin, Put}
import org.apache.hadoop.hbase.mapred.{ TableOutputFormat}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming._
import org.apache.spark.SparkConf
import org.apache.hadoop.conf.Configuration
import org.slf4j.LoggerFactory
/**
  * Created by farslan2 on 3/9/19.
  */
object App extends Serializable{

  val logger = LoggerFactory.getLogger(classOf[App])

  val tableName = "device_simulator"
  val columnFamilyDataBytes = Bytes.toBytes("cf")
  val columnDeviceIdBytes = Bytes.toBytes("deviceId")
  val columnTempBytes = Bytes.toBytes("temperature")
  val columnLatBytes = Bytes.toBytes("latitude")
  val columnLongBytes = Bytes.toBytes("longitude")
  val columnTsBytes = Bytes.toBytes("timestamp")


  object DataDevice extends Serializable {
    def getDevice(mssg: String): Device = {
      val gson = new Gson()
      gson.fromJson(mssg, classOf[Data]).data
    }


    def convertToPut(device: Device): Put = {//(ImmutableBytesWritable, Put) = {
      val rowkey = device.deviceId + "_" + device.timestamp
      val put = new Put(Bytes.toBytes(rowkey))
      val timeFormat = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX")).format(device.timestamp)

      put.add(columnFamilyDataBytes, columnDeviceIdBytes, Bytes.toBytes(device.deviceId))
      put.add(columnFamilyDataBytes, columnTempBytes, Bytes.toBytes(device.temperature))
      put.add(columnFamilyDataBytes, columnLatBytes, Bytes.toBytes(device.latitude))
      put.add(columnFamilyDataBytes, columnLongBytes, Bytes.toBytes(device.longitude))
      put.add(columnFamilyDataBytes, columnTsBytes, Bytes.toBytes(timeFormat))

      //(new ImmutableBytesWritable(Bytes.toBytes(rowkey)), put)
      put

    }


  }

    def main(args: Array[String]) {

      val props : Properties = new PropertyReader().readPropertyFile()


      logger.info(props.getProperty("com.iot.processor.spark.app.name"))
      logger.info(props.getProperty("com.iot.processor.spark.master"))
      val sparkConf = new SparkConf()
        .setAppName(props.getProperty("com.iot.processor.spark.app.name")).setMaster(props.getProperty("com.iot.processor.spark.master"))



      sparkConf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      sparkConf.registerKryoClasses(Array(classOf[org.apache.hadoop.hbase.io.ImmutableBytesWritable]))
      sparkConf.set("spark.kryo.referenceTracking", "false")




      val ssc = new StreamingContext(sparkConf, Seconds(5))
          ssc.checkpoint(props.getProperty("com.iot.processor.spark.checkpoint.dir"))


      val sc = ssc.sparkContext
      sc.setLogLevel("WARN")

      val topicsSet = props.getProperty("com.iot.simulator.kafka.topic").split(",").toSet
      var kafkaParams = Map[String, String](
        "zookeeper.connect" -> props.getProperty("com.iot.simulator.zookeeper"),
        "bootstrap.servers" -> props.getProperty("com.iot.simulator.kafka.boostrap.servers"),
        "auto.offset.reset" -> "smallest",
        "zookeeper.connection.timeout.ms" -> "1000")


      val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
        ssc, kafkaParams, topicsSet)


      val conf:Configuration = getHBaseConfig()
      val admin = new HBaseAdmin(conf)
      if (!admin.isTableAvailable(tableName)){
        logger.info(s"$tableName Table is not available, creating...")
        val tableDescriptor = new HTableDescriptor(tableName)
        tableDescriptor.addFamily(new HColumnDescriptor("cf".getBytes))
        admin.createTable(tableDescriptor)
      } else {
        logger.info(s"$tableName table is exists, skipping to creating table")
      }

      messages.map{mssg =>
        val device = DataDevice.getDevice(mssg._2)
        connection.put( DataDevice.convertToPut(device) )
      }.print()

      ssc.start()
      ssc.awaitTermination()
    }


  def getHBaseConfig() : Configuration = {
    val conf = HBaseConfiguration.create()
    conf.set(TableOutputFormat.OUTPUT_TABLE, tableName)
    conf
  }

  val conf = getHBaseConfig()
  val hCon = HConnectionManager.createConnection(conf)
  val connection = hCon.getTable(tableName)

}
