package com.iot.processor.util

import java.util.Properties


/**
  * Created by farslan2 on 3/9/19.
  */
class PropertyReader {

  private val prop = new Properties();

  @throws[Exception]
  def readPropertyFile(): Properties = {
    if (prop.isEmpty) {
      val input = classOf[PropertyReader].getClassLoader.getResourceAsStream("iot-simulator.properties")
      try
        prop.load(input)
      catch {
        case e: ClassNotFoundException =>
          throw e
      } finally if (input != null) input.close
    }
    prop
  }
}
