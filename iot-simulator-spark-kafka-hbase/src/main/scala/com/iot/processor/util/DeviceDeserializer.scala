package com.iot.processor.util

import java.util

import kafka.utils.VerifiableProperties
import org.apache.kafka.common.serialization.Deserializer

import com.google.gson.Gson

/**
  * Created by farslan2 on 3/9/19.
  */
class DeviceDeserializer extends Deserializer[Data]{

  def this(verifiableProperties: VerifiableProperties){this}

  override def configure(map: util.Map[String, _], b: Boolean): Unit = {}


  override def close(): Unit = {}

  override def deserialize(s: String, bytes: Array[Byte]): Data = {

    val gson = new Gson()

    gson.fromJson(new String(bytes), classOf[Data])
  }
}
