--hive
CREATE EXTERNAL TABLE hbase_device_simulator ( key string, deviceId string, temperature int, latitude float, longitude float, `timestamp` TIMESTAMP)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES ( "hbase.columns.mapping" = ":key,cf:deviceId,cf:temperature#b,cf:latitude#b,cf:longitude#b,cf:timestamp", "hbase.table.default.storage.type" = "binary", "serialization.format"="1" )
TBLPROPERTIES ("hbase.table.name" = "device_simulator");
--impala
INVALIDATE METADATA;

-- The maximum temperatures measured for every device.
SELECT deviceid, max(temperature) FROM hbase_device_simulator GROUP BY deviceid;

--The amount of data points aggregated for every device.
SELECT deviceid, count(*) FROM hbase_device_simulator GROUP BY deviceid;

--The highest temperature measured on a given day for every device.
--we can also use WHERE clause for a given date as such WHERE given_a_day = <your_day>
select deviceid,extract(`timestamp` ,"day") as given_a_day, max(temperature)
FROM hbase_device_simulator
group by deviceid, given_a_day;