<h5>Kafka start, list and create topic</h5>

```bin/kafka-server-start.sh config/server.properties```

```bin/kafka-topics.sh --list --zookeeper localhost:2181```

```bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test```


<h5>Spark Submit</h5>
Optionally ```--conf "spark.driver.extraClassPath=/etc/hbase/conf/"``` can be used in spark submit

```spark-submit --master <your-option> jar file```




<h5>VM Usage from Host

 Port binding to localhost from CDH VM
 
    Devices > Network Settings > Advanced
 
 Add below line to /etc/hosts
`127.0.0.1	quickstart.cloudera`

Recommended VirtualBox Configurations
1. Right click on the VirtualMachine and click Settings
2. Setup the VM to allow you to copy and paste from that machine to your local and vice-versa

    Click on General -> Advanced
    
    Set Shared Clipboard to Bidirectional

3. Setup port forwarding from port 2222 to port 22 to allow SSH to the machine

    Click on Network -> Advanced -> Port Forwarding
    
    Add a new entry
    
    Name: 2222
    
    Host Port: 2222
    
    Guest Port: 22
    
<h5>SSH and SCP to VM</h5>
`ssh -p 2222 cloudera@localhost`

`scp -P 2222 {PATH_TO_FILE_ON_LOCAL} cloudera@localhost:{DESTINATION_PATH_ON_VM}`

<h5>Hive table creation based on HBase tables</h5>

Use `resources/hbase_hive.sql` hql in HUE(Hive) or console `hive` to create table
 
after creation table in hive, use `INVALIDATE METADATA` in impala to use the table.

